#!/bin/sh
git clone --depth 1 https://gitlab.com/VadikLeshy/sib_utils.git
git clone --depth 1 https://gitlab.com/KawaiiGraphics/Kawaii3D.git
git clone --depth 1 https://gitlab.com/KawaiiGraphics/KawaiiRenderer.git
git clone --depth 1 https://gitlab.com/KawaiiGraphics/MisakaRenderer.git
git clone --depth 1 https://gitlab.com/KawaiiGraphics/KawaiiFigures3D.git
git clone --depth 1 https://gitlab.com/KawaiiGraphics/KawaiiAssimp.git
git clone --depth 1 https://gitlab.com/KawaiiGraphics/Kawaii3D-Samples.git

